# -*- coding: utf-8 -*-
"""
Created on Thu Oct  7 10:04:49 2021

@author: Jaymal.Kalidas
"""


import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)

import numpy as np

# data visualization
import seaborn as sns
%matplotlib inline
from matplotlib import pyplot as plt
from matplotlib import style

from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn import metrics

from sklearn.ensemble import RandomForestClassifier

from functions.cm_plot import plot_confusion_matrix
from functions.reduce_levels import reduce_levels_to_top_N

from functions.actors_feature import reduce_title_principal_to_train_data

from functions.actors_feature import feature_impact_actor
from functions.actors_feature import feature_growing_star
from functions.actors_feature import feature_actor_actress_ratio
from functions.actors_feature import feature_cast_size

import topic_modelling_function as tp


##############################CLEAN DATA##############################
movies = pd.read_csv('data/IMDb movies.csv')
movies_small = movies[['imdb_title_id','title','year','budget','duration','writer'
                       ,'director','production_company'
                       ,'actors','avg_vote','votes','country','language','genre'
                       ,'date_published','description']]


#working with good volume of votes 
movies_small = movies_small[movies_small['votes'] > 200]

#####Define target - avg votes >=7 (better balance than 8)
movies_small['Target'] = np.where(
    movies_small['avg_vote'] >= 7 , 1, 0)
movies_small['Target'].value_counts()
### Class is imbalanced

#drop actor col for now
movies_small = movies_small.drop(columns ='actors')
movies_small = movies_small.drop(columns ='title')

####Basic cleaning
movies_small['year'] = pd.to_numeric(movies_small['year'], errors = 'coerce')

#drop the budget col - too many nans
movies_small = movies_small.drop(columns ='budget')

##############################Features##############################


movies_small = reduce_levels_to_top_N(movies_small, 'genre', 20)
movies_small = reduce_levels_to_top_N(movies_small, 'writer', 20)
movies_small = reduce_levels_to_top_N(movies_small, 'director', 20)
movies_small = reduce_levels_to_top_N(movies_small, 'production_company', 20)
#movies_small = reduce_levels_to_top_N(movies_small, 'actors', 100)
movies_small = reduce_levels_to_top_N(movies_small, 'country', 10)
movies_small = reduce_levels_to_top_N(movies_small, 'language', 10)

#month of the year released

movies_small['date_published'] = pd.to_datetime(movies_small['date_published'],errors ='coerce')
movies_small['month'] = movies_small['date_published'].dt.month
movies_small['day'] = movies_small['date_published'].dt.day
movies_small.dtypes

#label encode
features_to_encode = ['genre','writer','director','production_company','country','language']
#features_to_encode = ['genre','country','language']
movies_small = pd.get_dummies(movies_small, columns = features_to_encode)


#Features to build
##is part of sequal [second or third movie faeture]
##gripping word in title feature



#Drop nas
movies_small = movies_small.dropna()


#drop day col
#movies_small = movies_small.drop(columns = ['day','month'])
#movies_small = movies_small.drop(columns=['writer'])

######################Features built on all data###############
title_principal_path = 'data\IMDb title_principals.csv'
actor_actress = feature_actor_actress_ratio(title_principal_path)
movies_small = pd.merge(movies_small,actor_actress,
                        on='imdb_title_id'
                        ,how='left')

movies_small = movies_small.fillna({'actor_ratio':0
                                    ,'only_actors':0
                                    ,'only_actress':0})

cast_size = feature_cast_size(title_principal_path)
movies_small = pd.merge(movies_small,cast_size,
                    on='imdb_title_id'
                    ,how='left')

movies_small = movies_small.fillna({'cast_size':0})

######################Features built on all data###############







##############################TRAIN/TEST##############################




##Sort ascending the data by date
movies_small = movies_small.sort_values(by=['date_published'])
movies_small = movies_small.drop(columns=['date_published'])

#Split train and test
x_train, x_test= np.split(movies_small, [int(.70 *len(movies_small))])

y_train = x_train['Target']
y_test = x_test['Target']

x_train = x_train.drop(columns=['Target'])
x_test = x_test.drop(columns=['Target'])

####Train test split
#y = movies_small['Target']
#x = movies_small.drop(columns = 'Target')
#x_train, x_test, y_train, y_test = train_test_split(x, y, test_size =0.3
#                                                    ,random_state = 123)



######################Features built on training data only###############
##Join Actors feature
title_principal_path = 'data\IMDb title_principals.csv'
title_principal_final = feature_impact_actor(title_principal_path, movies_small, x_train)
x_train = pd.merge(x_train,title_principal_final,
                        on='imdb_title_id'
                        ,how='left')

x_train = x_train.fillna({'experiance':0
                          ,'impact_actor_high':0
                          ,'impact_actor_medium':0
                          ,'impact_actor_low':0})

x_test = pd.merge(x_test,title_principal_final,
                        on='imdb_title_id'
                        ,how='left')
x_test = x_test.fillna({'experiance':0
                          ,'impact_actor_high':0
                          ,'impact_actor_medium':0
                          ,'impact_actor_low':0})

## join improvement indicator
title_principal_improvement_indicator = feature_growing_star(
    title_principal_path, movies_small, x_train)

x_train = pd.merge(x_train,title_principal_improvement_indicator,
                        on='imdb_title_id'
                        ,how='left')

x_train = x_train.fillna({'improvement_indicator':0})

x_test = pd.merge(x_test,title_principal_improvement_indicator,
                        on='imdb_title_id'
                        ,how='left')
x_test = x_test.fillna({'improvement_indicator':0})




## join principal cat average
#x_train = pd.merge(x_train,title_principal_category_avg,
#                         on='imdb_title_id'
#                         ,how='left')

# x_test = pd.merge(x_test,title_principal_category_avg,
#                         on='imdb_title_id'
#                         ,how='left')



################Topic modelling###############
df1,df2,df3,lda_model,dictionary = tp.topic_model(    
    data=x_train.head(100),
    text_col='description',
    no_topics=15,
    stem_lemmatize_function='lemmatize',
    unique_identifier='imdb_title_id',
    per_word_topics=True,
    ngram_no=2)
x_train = df1

x_test = x_test.head(100)

x_test['tokens'] = x_test['description'].apply(tp.tokenize)
x_test['tokens'] = x_test['tokens'].apply(tp.lemmatize)
x_test['ngrams'] = x_test['tokens'].apply(tp.make_ngrams, ngram_no=2)


input = []
input.append(x_test['ngrams'][45270])


data = x_test
unique_identifier = 'description'
id2word = dictionary
df_topics = pd.DataFrame()
for i in range(len(data)):
    
    #for i in range(50000,50005):
    if i % 100 == 0:
        print(f'Progress: {(i+1)} of {len(data)} records processed')
    text = data['ngrams'].iloc[i]
    unique_id = data[unique_identifier].iloc[i]
    
    bow = id2word.doc2bow(text)
    #must update this with the infer_topics
    #df = pd.DataFrame(lda_model.get_document_topics(bow)).transpose().tail(1)
    df = pd.DataFrame(lda_model.get_document_topics(bow),columns=['Topic','Prob']).transpose()
    df = pd.DataFrame(data=[list(df.iloc[1])],columns=list(df.iloc[0].astype(int)))
    
    #df[unique_identifier] = data[unique_identifier].iloc[i]
    df = df.assign(**{unique_identifier: unique_id})
    df_topics = pd.concat([df_topics,df],axis=0)
    
# Find most likely Topic and probability for each document
print('Apply topic probability filtering and finding top matches ...')
df_topics = df_topics.reset_index()
df_topics = df_topics.drop(columns=['index'])
df_topics['Best Topic'] = df_topics[df_topics.columns[~df_topics.columns.isin([unique_identifier,'Best Topic','Best Topic Probability'])]].idxmax(axis=1)
df_topics['Best Topic Probability'] = df_topics[df_topics.columns[~df_topics.columns.isin([unique_identifier,'Best Topic','Best Topic Probability'])]].max(axis=1)
     
data = pd.merge(left=data, right=df_topics, how="left",on=unique_identifier)
        
x_test = data    
################Topic modelling###############


## Fill all nas with zero
x_train= x_train.fillna(0)
x_test= x_test.fillna(0)


##Drop cols that make up the target and id fields
x_train = x_train.drop(columns =['votes','avg_vote','imdb_title_id'
                                 ,'description'
                                 #,'ngrams','tokens'
                                 ])
x_test = x_test.drop(columns =['votes','avg_vote','imdb_title_id'
                               ,'description'
                               #,'ngrams','tokens'
                               ])


######################Features built on training data only###############



##############################MODEL##############################

#Random forest
rf_classifier = RandomForestClassifier(
                      min_samples_leaf=5,
                      n_estimators=500,
                      bootstrap=True,
                      #oob_score=True,
                      #n_jobs=-1,
                      class_weight = 'balanced',
                      random_state=123,
                      max_features='auto')

#Train
rf_classifier.fit(x_train, y_train)

#Predict
y_pred = rf_classifier.predict(x_test)
y_pred_probs = rf_classifier.predict_proba(x_test)


##############################Accuracy##############################

#training score
rf_classifier.score(x_train, y_train)

cm = confusion_matrix(y_test, y_pred)

plot_confusion_matrix(cm  = cm, 
                      normalize    = False,
                      target_names = ['0', '1'],
                      title        = "Confusion Matrix")


##Accuracy stats
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score

print('accuracy: ' + str( metrics.accuracy_score(y_test, y_pred)))
print('precision_score: ' + str(precision_score(y_test, y_pred)))
print('recall_score: ' + str(recall_score(y_test, y_pred)))
print('f1_score: ' + str(f1_score(y_test, y_pred)))


from matplotlib import pyplot
# get importance
importance = rf_classifier.feature_importances_
importance_df = pd.DataFrame(importance)
importance_df = importance_df.set_index( x_train.columns).reset_index()
importance_df =importance_df.rename(columns={'index':'feature'})
# summarize feature importance
#for i,v in enumerate(importance):
#	print('Feature: %0d, Score: %.5f' % (i,v))
# plot feature importance
#pyplot.bar([x for x in range(len(importance))], importance)
#pyplot.show()
