# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 08:20:10 2021

@author: Jaymal.Kalidas
"""

import pandas as pd

movies = pd.read_csv('data/IMDb movies.csv')
movies_small = movies[['imdb_title_id','title','year','budget','duration','imdb_name_id']]

ratings = pd.read_csv('data/IMDb ratings.csv')
ratings_small = ratings[['imdb_title_id','total_votes','weighted_average_vote',
                         'allgenders_18age_avg_vote','allgenders_18age_votes',
                         'allgenders_30age_avg_vote','allgenders_30age_votes',
                         'allgenders_45age_avg_vote','allgenders_45age_votes',
                         'females_allages_avg_vote',
                         'males_allages_avg_vote'
                         ,'allgenders_0age_avg_vote']]

names = pd.read_csv('data/IMDb names.csv')

title_principals = pd.read_csv('data/IMDb title_principals.csv')

characters = pd.read_csv('data/characters.csv')

marvel_dc_characters = pd.read_excel('data/marvel_characters_info.xlsx')
#marvel_dc_characters[['shortName', 'detailName']] = marvel_dc_characters['Name'].str.split('(', 1, expand=True)
marvel_dc_characters_names = marvel_dc_characters['Name']
names_marvel_dc = names[names.name.str.contains('|'.join(marvel_dc_characters_names),na=False)]

movies_marvel_dc =pd.merge(names_marvel_dc, title_principals, how = 'left', on = 'imdb_name_id')
movies_marvel_dc =pd.merge(movies_marvel_dc, movies_small, how = 'left', on = 'imdb_title_id')

movies_marvel_dc_dedup = movies_marvel_dc.drop_duplicates() 

marvel = movies[movies['production_company'] == 'Marvel Studios']
marvel = movies[movies['production_company'] == 'Marvel Studios']
dc = movies[movies['production_company'] == 'DC Entertainment']

prod_comp = movies['production_company'].value_counts()

core_data = pd.merge(movies_small,ratings_small, how = 'left', on = 'imdb_title_id')
core_data['year'].value_counts()
core_data = pd.DataFrame(core_data.budget.str.split(' ',1).tolist(),
                                 columns = ['currency','value'])


title_principals_characters = title_principals[['characters']]

title_principals_characters["new_column"] = title_principals_characters['characters'].str.replace('[^\w\s]','')

mylist = ['CJ Callum','Miss Lillian Travers','Bruce Wayne']

title_principals_characters_movies_of_interest = title_principals_characters[title_principals_characters['new_column'].isin(mylist)]


prod_comp = movies['production_company'].value_counts()

### Data basics series
## we can look at monetary value
# we can look at ratings
# we can look at if it appealed to a certain audience
# we could create a new variable that takes into account a few variables to determine 'the best' or 'blockbuster'
#this could be a nice way to create a target variable without anyone knowing that it will be our target variable

#Which were the top movies of 2016
#Who were the top producers of 2016
#Who were the top actors and actresses from 2010 to 2016


### Data story telling
#Avengers story - you want to motivate for the next series - what are the items that made the series successful
#Actors that worked together
#Growth movie by movie
#Genres the seires touched
#What made them unique compared to others



### ML Series
## Can we predict if a movie in will be a blockbuster
## Can we predict the rating
## Can we predict the revenue



###Ratings data
import matplotlib.pyplot as plt
ratings_small = ratings_small[ratings_small['total_votes' ] >= 200]

ratings_small.plot.scatter(x=ratings_small['allgenders_18age_avg_vote'], y=ratings_small['allgenders_30age_avg_vote'], title= "Scatter plot between two variables X and Y");

plt.scatter(ratings_small['allgenders_18age_avg_vote'], ratings_small['allgenders_45age_avg_vote'])
ratings_small['allgenders_18age_avg_vote'].corr(ratings_small['allgenders_30age_avg_vote'] )
ratings_small['allgenders_18age_avg_vote'].corr(ratings_small['allgenders_45age_avg_vote'])
ratings_small['allgenders_30age_avg_vote'].corr(ratings_small['allgenders_45age_avg_vote'])
ratings_small['females_allages_avg_vote'].corr(ratings_small['males_allages_avg_vote'])

#https://www.kaggle.com/saurav9786/imdb-score-prediction-for-movies

