# -*- coding: utf-8 -*-
"""
Created on Fri Oct 22 08:20:23 2021

@author: Jaymal.Kalidas
"""
def f(x):
    return x*x


def get_person_relations(i, title_principal_poi_sample
                         ,temp_title_principal_name
                         ,temp_title_principal_movie):
    
    current_person = title_principal_poi_sample['imdb_name_id'][i]    
    person = temp_title_principal_name.loc[current_person]
    #step 2
    person_movie = temp_title_principal_movie.loc[person['imdb_title_id']]
    person_movie = person_movie[person_movie['imdb_name_id'] != current_person]
    person_movie['person'] = current_person
    print(i)

    return person_movie

result = []
results = []
def collect_result(result):
    #global results
    results.append(result)