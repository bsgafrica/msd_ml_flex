# -*- coding: utf-8 -*-
"""
Created on Wed Oct 20 16:25:42 2021

@author: Greg.Desilla
"""
import pandas as pd
import gensim.corpora as corpora
import gensim
import string
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer  
import nltk
from nltk.util import ngrams

nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')
# EXAMPLE USAGE:
#import topic_modelling_function as tp
#df1,df2,df3,lda_model,dictionary = tp.topic_model(
#        data=df,
#        text_col='description',
#        no_topics=25,
#        tokenize_function=tp.tokenize_and_lemmatize,
#        unique_identifier='id',
#        per_word_topics=True)


nltk.download('wordnet')
stemmer = PorterStemmer()
lemmatizer = WordNetLemmatizer()

def stem(tokens):
     
    # now stem the tokens
    tokens = [stemmer.stem(token) for token in tokens]

    return tokens

def tokenize(text):
    text = str.lower(text)
    tokens = nltk.tokenize.word_tokenize(text)
    # strip out punctuation and make lowercase
    tokens = [token.lower().strip(string.punctuation)
              for token in tokens if token.isalnum()]

    #remove stopwords
    # Check characters to see if they are in punctuation
    tokens = [char for char in text if char not in string.punctuation]
    # Join the characters again to form the string.
    tokens = ''.join(tokens) 
    # Now just remove any stopwords
    tokens = [word for word in tokens.split() if word.lower() not in stopwords.words('english')]

    return tokens


def lemmatize(tokens):
    """
    Part-of-speech constants
    ADJ, ADJ_SAT, ADV, NOUN, VERB = 'a', 's', 'r', 'n', 'v'
    """   
    def get_wordnet_pos(word):
        """Map POS tag to first character lemmatize() accepts"""
        tag = nltk.pos_tag([word])[0][1][0].upper()
        tag_dict = {"J": wordnet.ADJ,
                    "N": wordnet.NOUN,
                    "V": wordnet.VERB,
                    "R": wordnet.ADV}
        
        return tag_dict.get(tag, wordnet.NOUN)
    
    # now lemmatize the tokens
    #tokens = [lemmatizer.lemmatize(token,pos=part_of_speech) for token in tokens]
    tokens = [lemmatizer.lemmatize(w, get_wordnet_pos(w)) for w in tokens]

    return tokens

def stem_and_lemmatize(tokens, part_of_speech='n'):
       
    # now stem the tokens
    tokens = [stemmer.stem(token) for token in tokens]
    
    # now lemmatize the tokens
    tokens = [lemmatizer.lemmatize(token,pos=part_of_speech) for token in tokens]

    return tokens

def make_ngrams(texts,ngram_no):
        ng = list(ngrams(texts, ngram_no))
        tup_list =[] 
        for item in ng:           
            concat_ngram ='_'.join(item)
            tup_list.append(concat_ngram)
        return tup_list

def topic_model(data,text_col,unique_identifier, no_topics,stem_lemmatize_function='stem',minimum_probability=0.01, per_word_topics=False, ngram_no=2):
    """Create a topic model given a set of data
    :param data: Input dataframe
    :type data: pandas dataframe
    :param text_col: The text column on which topic modelling will be performed
    :type text_col: string
    :param no_topics: How many topics you would like to try generate
    :type no_topics: integer
    :param tokenize_function: How you want to tokenize your text. Options are: 
        (1)tokenize_and_lemmatize
        (2)tokenize_and_stem
        (3)tokenize_and_stem_and_lemmatize
    :type tokenize_function: func
    :param unique_identifier: Unique identifier to join topics back to original dataframe
    :type unique_identifier: string
    :param minimum_probability: Minimum probability that topics will need to exceed or will be dropped
    :type minimum_probability: float
    :param per_word_topics: Generate topic per word or not
    :type per_word_topics: boolean
    
    
    :returns: df1 -- original dataframe with tokens, topics
    :returns: df2 -- dataframe with tokens, topics
    :returns: df3 -- topics generated
    """
    #clean data
    print(f'Cleaning null data in column: {text_col}')
    print(f'Original dataframe size: {len(data)}')
    data = data.dropna(subset=[text_col])
    print(f'New dataframe size: {len(data)}')
    
    #create tokenised field 
    print('Tokenizing data ...')
    data['tokens'] = data[text_col].apply(tokenize)
       
    try: 
        if stem_lemmatize_function == 'stem':
            print(f'Applying stemming function ...')
            data['tokens'] = data['tokens'].apply(stem)
        elif stem_lemmatize_function == 'lemmatize':
            print(f'Applying lemmatizing function ...')
            data['tokens'] = data['tokens'].apply(lemmatize)
        elif stem_lemmatize_function == 'stem_and_lemmatize':
            print(f'Applying stemming and lemmatizing function ...')
            data['tokens'] = data['tokens'].apply(stem_and_lemmatize)
    except:
        print('Failed to provide method. Should be "stem","lemmatize"')
       
    print('Creating {ngram_no} ngrams from data ...')
    data['ngrams'] = data['tokens'].apply(make_ngrams,ngram_no=2)
   
    # Create Dictionary
    print('Creating Dictionary ...')
    data_lemmatized = data['ngrams']
    id2word = corpora.Dictionary(data_lemmatized)

    # Create Corpus
    print('Creating Corpus ...')
    texts = data_lemmatized

    # Term Document Frequency
    print('Creating Term Document Frequency ...')
    corpus = [id2word.doc2bow(text) for text in texts]

    # View
    #print(corpus[:1])
    
    # Train LDA model
    print('Training LDA model ...')
    lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                          id2word=id2word,
                                          num_topics=no_topics, 
                                          random_state=100,
                                          update_every=1,
                                          chunksize=100,
                                          passes=5,
                                          alpha='auto',
                                          per_word_topics=per_word_topics,
                                          minimum_probability=minimum_probability)
    
    # Get_document_topics for a document with a single token 'user'
    print('Applying LDA model to data ...')
    df_topics = pd.DataFrame()
    for i in range(len(data)):
    #for i in range(50000,50005):
        if i % 100 == 0:
            print(f'Progress: {(i+1)} of {len(data)} records processed')
        text = data['ngrams'].iloc[i]
        unique_id = data[unique_identifier].iloc[i]
        
        bow = id2word.doc2bow(text)
        #must update this with the infer_topics
        #df = pd.DataFrame(lda_model.get_document_topics(bow)).transpose().tail(1)
        df = pd.DataFrame(lda_model.get_document_topics(bow),columns=['Topic','Prob']).transpose()
        df = pd.DataFrame(data=[list(df.iloc[1])],columns=list(df.iloc[0].astype(int)))
        
        #df[unique_identifier] = data[unique_identifier].iloc[i]
        df = df.assign(**{unique_identifier: unique_id})
        df_topics = pd.concat([df_topics,df],axis=0)
    
    # Find most likely Topic and probability for each document
    print('Apply topic probability filtering and finding top matches ...')
    df_topics = df_topics.reset_index()
    df_topics = df_topics.drop(columns=['index'])
    df_topics['Best Topic'] = df_topics[df_topics.columns[~df_topics.columns.isin([unique_identifier,'Best Topic','Best Topic Probability'])]].idxmax(axis=1)
    df_topics['Best Topic Probability'] = df_topics[df_topics.columns[~df_topics.columns.isin([unique_identifier,'Best Topic','Best Topic Probability'])]].max(axis=1)
     
    df_all = pd.merge(left=data, right=df_topics, how="left",on=unique_identifier)
        
    lda_topics = pd.DataFrame(lda_model.print_topics())
    
    print('Topic modelling complete')  
    
    return df_all, df_topics, lda_topics, lda_model, id2word

def infer_topics(texts_list, ldamodel, dictionary):
       
    other_corpus = [dictionary.doc2bow(text) for text in texts_list]
    unseen_doc = other_corpus[0]
    infer = ldamodel[unseen_doc][0]  # get topic probability distribution for a document
    return infer

