# -*- coding: utf-8 -*-
"""
Created on Thu Oct 21 15:02:46 2021

@author: Jaymal.Kalidas
"""

from howmany_within_range import howmany_within_range

import multiprocessing as mp
if __name__ == '__main__':
   
    print("Number of processors: ", mp.cpu_count())
    
    # Step 1: Init multiprocessing.Pool()
    pool = mp.Pool(mp.cpu_count())
    
    # Step 2: `pool.apply` the `howmany_within_range()`
    results = [pool.apply(howmany_within_range, args=(row, 4, 8)) for row in data]
    
    # Step 3: Don't forget to close
    pool.close()    
    
    print(results[:10])