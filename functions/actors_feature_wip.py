# -*- coding: utf-8 -*-
"""
Created on Wed Oct 20 08:27:57 2021

@author: Jaymal.Kalidas
"""
import pandas as pd

####################Average rating per principal category####################
#Read in title principal
title_principal = pd.read_csv('data/IMDb title_principals.csv')

#Reduce to training data only
title_principal = reduce_title_principal_to_train_data('data/IMDb title_principals.csv',movies_small, x_train)


#Average rating per person on the movie
title_principal_average_rating = title_principal.groupby('imdb_name_id').agg({
    'avg_vote':['mean','count'],
    }).reset_index()

title_principal_average_rating.columns = [''.join(col).strip() for col in title_principal_average_rating.columns.values]

##Reduce the data to people that have done this over 5 times
title_principal_average_rating = title_principal_average_rating[
    title_principal_average_rating['avg_votecount'] > 5]

title_principal_average_rating = title_principal_average_rating.rename(
    columns={'avg_votemean':'avg_vote'})
title_principal_average_rating = title_principal_average_rating[['imdb_name_id',
                                                                 'avg_vote']]



##Read in the full title principal including test data
title_principal = pd.read_csv('data/IMDb title_principals.csv')
#select the title, name and category
title_principal = title_principal[['imdb_title_id','imdb_name_id','category']]
#join trianing data votes
title_principal  = pd.merge(title_principal,
                            title_principal_average_rating,
                            how = 'left',
                            on = 'imdb_name_id')

title_principal.fillna({'avg_vote':0})

## Group them in their categories per movie
## average per category
title_principal_category_avg = title_principal.groupby(['imdb_title_id',
                                                       'category']).agg({
                                                           'avg_vote':'mean'
                                                           }).reset_index()


                                                           
##Remove unnessary roles that dont occur a lot
                                                           
                                                           
title_principal_category_avg = title_principal_category_avg.pivot(
    index='imdb_title_id', columns='category', values='avg_vote').reset_index()

title_principal_category_avg =title_principal_category_avg[[
'imdb_title_id'    
,'actor'
,'director'             
,'actress'                
,'writer'                 
,'producer'               
,'composer'               
,'cinematographer']]

title_principal_category_avg = title_principal_category_avg.fillna(0)

####################Average rating per principal category####################











####################People collaboration Feature####################
#Create a counter of how many times these individuals have worked together

title_principal = pd.read_csv('data/IMDb title_principals.csv')

#Reduce to training data only
title_principal = reduce_title_principal_to_train_data('data/IMDb title_principals.csv',movies_small, x_train)

#Reduce people to those that have appeared more than 5 times
title_principal_poi = title_principal.groupby('imdb_name_id').agg({
    'avg_vote':'count',
    }).reset_index()

##Reduce the data to people that have done this over 5 times
title_principal_poi = title_principal_poi[
    title_principal_poi['avg_vote'] > 5]


title_principal = title_principal[title_principal[
    'imdb_name_id'].isin(title_principal_poi['imdb_name_id'])]

title_principal = title_principal[['imdb_title_id','imdb_name_id']]

#title_principal = title_principal.to_numpy()

collab_matrix = pd.DataFrame()
temp_df = pd.DataFrame()
i = 0
#current_person =  title_principal_poi['imdb_name_id'][0]
for current_person in title_principal_poi['imdb_name_id']:
    if i % 100 == 0:
        print(i)
        
    person= np.where(title_principal==current_person)
#    person = title_principal[title_principal[
#    '1'] == current_person]
    
    person_movie = title_principal[person[0]]

    all_movies_index = np.where(title_principal==person_movie[0])

    all_movies = title_principal[all_movies_index[0]]
    i = i + 1
    
    all_movies = all_movies[all_movies != current_person]
   # person_movie = title_principal[
   #     title_principal['imdb_title_id'].isin(person['imdb_title_id'])]
    
    person_movie = person_movie[person_movie['imdb_name_id'] != current_person]['imdb_name_id']
    
    temp_df['other_people'] = person_movie 
    temp_df['person'] = current_person 
    
    collab_matrix = pd.concat([collab_matrix,temp_df])
    i = i + 1

collab_matrix = pd.DataFrame()
temp_title_principal_name =title_principal.set_index('imdb_name_id')
temp_title_principal_movie =title_principal.set_index('imdb_title_id')
i = 0
for current_person in title_principal_poi['imdb_name_id']:
    i = i +1
    
    t0 = pd.Timestamp.now()
    person = temp_title_principal_name.loc[current_person]
    t1 = pd.Timestamp.now()
    #Get the persons movies and relating actors
    person_movie = temp_title_principal_movie.loc[person['imdb_title_id']]
    t2 = pd.Timestamp.now()
    #Remove the person we are looking at from the dataset
    person_movie = person_movie[person_movie['imdb_name_id'] != current_person]
    t3 = pd.Timestamp.now()
    person_movie['person'] = current_person
    t4 = pd.Timestamp.now()
    collab_matrix = pd.concat([collab_matrix,person_movie])
    t5 = pd.Timestamp.now()
    if i % 1000 == 0:
        print('{} of {}'.format(i,len(title_principal_poi['imdb_name_id'])))
#        print('step1: ' +str(t1-t0))
#        print('step2: ' +str(t2-t1))
 #       print('step3: ' +str(t3-t2))
 #       print('step4: ' +str(t4-t3))
 #       print('step5: ' +str(t5-t4))


temp = title_principal.loc[title_principal['imdb_name_id'] == current_person]      
#this is okay
#temp = title_principal['imdb_title_id'].where(title_principal['imdb_name_id'] == current_person)
   

########Very slow process
title_principal = title_principal[title_principal[
    'imdb_name_id'].isin(title_principal_poi['imdb_name_id'])]

collab_matrix = pd.DataFrame()
i = 0
for current_person in title_principal_poi['imdb_name_id']:
    if i % 100 == 0:
        print(i)
    person = title_principal[title_principal[
    'imdb_name_id'] == current_person]
    #Get the persons movies and relating actors
  +
    #Remove the person we are looking at from the dataset
    person_movie = person_movie[person_movie['imdb_name_id'] != current_person]
    person_movie_group = person_movie.groupby('imdb_name_id').agg({
        'imdb_title_id':'count'}).reset_index()
    
    person_movie_group = person_movie_group.rename(
        columns={'imdb_title_id':'count_collab'})
    
    #remove interactions that have only happened once
    person_movie_group = person_movie_group[person_movie_group['count_collab'] > 1]
    
    person_movie_group['person'] = current_person
    
    #pivot the data
    person_movie_group = person_movie_group.pivot(
        index='person', columns='imdb_name_id', values='count_collab').reset_index()
    
    collab_matrix = pd.concat([collab_matrix,person_movie_group])
    i = i + 1



####################People collaboration Feature####################





